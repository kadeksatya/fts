<?php

namespace Database\Seeders;

use App\Models\Month;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MonthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Month::insert([
            0 => [
                'name'=>'Januari'
            ],

            1 => [
                'name'=>'Februari'
            ],
            2 => [
                'name'=>'Maret'
            ],
            3 => [
                'name'=>'April'
            ],
            4 => [
                'name'=>'Mei'
            ],
            5 => [
                'name'=>'Juni'
            ],
            6 => [
                'name'=>'Juli'
            ],
            7 => [
                'name'=>'Agustus'
            ],
            8 => [
                'name'=>'September'
            ],
            9 => [
                'name'=>'Oktober'
            ],
            10 => [
                'name'=>'November'
            ],
            11 => [
                'name'=>'Desember'
            ]
            ]);
    }
}
