<?php

namespace Database\Seeders;

use App\Models\Dataset;
use App\Models\Datasetss;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatasetsSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dataset::insert(
            [
            [
                'month' => 'Januari',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Februari',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Maret',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'April',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Mei',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Juni',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Juli',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Agustus',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'September',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Oktober',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'November',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Desember',
                'barang_id' => 1,
                'period' => 2018,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Januari',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Februari',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Maret',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'April',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Mei',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Juni',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Juli',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Agustus',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'September',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Oktober',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'November',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Desember',
                'barang_id' => 1,
                'period' => 2019,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Januari',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Februari',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Maret',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'April',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Mei',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Juni',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Juli',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Agustus',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'September',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Oktober',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'November',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ],
            [
                'month' => 'Desember',
                'barang_id' => 1,
                'period' => 2020,
                'value' => rand(1, 999)
            ]
            ]
        );
    }
}
