<?php

namespace Database\Seeders;

use App\Models\Barang;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Barang::insert([
            [
                'unit' => 'sak',
                'name' => 'Beras'
            ],
            [
                'unit' => 'botol',
                'name' => 'Minyak'
            ],
            [
                'unit' => 'pcs',
                'name' => 'Masako'
            ]
        ]);
    }
}
