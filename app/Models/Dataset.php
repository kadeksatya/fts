<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Dataset extends Model
{
    use HasFactory;

    protected $guarded = [];


    /**
     * Get the barang that owns the Dataset
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function barang(): BelongsTo
    {
        return $this->belongsTo(Barang::class, 'barang_id');
    }

    protected $ui = 0;

    public function setUi($u) {
    	$this->ui = $u;
    }

    public function getUi() {
    	return $this->ui;
    }
}
