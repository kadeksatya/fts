<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Dataset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $data['total_barang'] = Barang::count();
        $data['data_barang'] = Dataset::groupBy('month')
       
       ->selectRaw('sum(value) as total_value, month, MONTH(created_at) months')
       ->orderBy('months', 'ASC')
       ->pluck('total_value','month');



        return view('admin.dashboard.index',[
            'page_name' => 'Dashboard',
            'url_add' => null
        ], $data);


    }
}
