<?php

namespace App\Http\Controllers;

use App\Models\Dataset;
use App\Models\Barang;
use App\Models\Datasetss;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResultController extends Controller
{
    public function index()
    {
        $years = range(date('Y'), 2030);
        $bahan = Barang::all();

        return view('admin.hasil.index', [
            'page_name' => 'Hasil',
            'year' => $years,
            'bahan' => $bahan,
            'url_add' => null
        ]);
    }

    public function fuzzifikasi($dataset, $interval, $orde)
    {
      $i=0;


      foreach ($dataset as $key) {
        $y=0;
        if ($i>=$orde) {
            foreach ($interval as $key2) {
              if ( $key->value >= $key2['bawah'] && $key->value < $key2['atas']) {
                $fuzzyfikasi = $y;
              }
            $y++;
            }
          for ($n=0; $n < $orde; $n++) {
            $temp_orde['orde'.$n] = $data[$i-$orde+$n]['fuzzifikasi'];
          }
          
          $object = (object)($temp_orde);
          $data[$i]=['period'=>$key->period, 'month'=>$key->month, 'barang_name'=>$key->barang->name, 'value'=>$key->value, 'fuzzifikasi'=>$fuzzyfikasi,'orde'=>$object];

        }else{
          foreach ($interval as $key2) {
            if ( $key->value >= $key2['bawah'] && $key->value < $key2['atas']) {
              $fuzzyfikasi = $y;
            }
            $y++;
          }
        $data[$i]=['period'=>$key->period, 'month'=>$key->month,'barang_name'=>$key->barang->name,'value'=>$key->value, 'fuzzifikasi'=>$fuzzyfikasi,'orde'=>null];
        }
        $i++;
      }

  
      return $data;
    }


    public function model_chen($flr,$flrg)
    {
      $i=0;
      $hasil=[];
      foreach ($flrg as $key) {
        $temp=explode(",",$key);
        $z=0;
        foreach($temp as $link)
          {
            if($link == '')
            {
                unset($temp[$z]);
            }
            $z++;
          }
        $object = (object)($temp);
        $n=0;
        $hasil=[];
        foreach ($flr as $key2) {

          if (!is_null($key2['orde'])) {
            $temp_orde='';
          foreach ($key2['orde'] as $key3) {
            $temp_orde.=$key3.',';
            }
            if ($key==$temp_orde) {
              $hasil[$n]=$key2['fuzzifikasi'];
              $n++;
            }
          }
        }

        $hasil=array_unique($hasil);
        asort($hasil);
        $temp_hasil = $hasil;
        $hasil=[];
        $x=0;
        foreach ($temp_hasil as $key) {
          $hasil[$x]=$key;
          $x++;
        }
        $hasil_akhir = (object)($hasil);
        $chen[$i] = ['relasi'=>$object,'hasil'=>$hasil_akhir];
        $i++;
      }
      return $chen;
    }

    public function flrg_chen($flr)
    {
      $i=0;
      foreach ($flr as $key) {
        $temp='';
        if (!is_null($key['orde'])) {
          foreach ($key['orde'] as $key2) {
            $temp.=$key2.',';
          }
          $flrg[$i]=$temp;
          $i++;
        }

      }

      $flrg=array_unique($flrg);
      asort($flrg);
      $temp = $flrg;
      $flrg = [];
      $i=0;
      foreach ($temp as $key) {
        $flrg[$i]=$key;
        $i++;
      }
      $hasil=$this->model_chen($flr,$flrg);
      return $hasil;
    }



    public function defuzzifikasi_chen($flrg,$interval)
    {
      $n=0;
      foreach ($flrg as $key) {
        $relasi = $key['relasi'];
        $i=0;
        $temp=0;
        foreach ($key['hasil'] as $key2) {
          $temp+=$interval[$key2]['median'];
          $i++;
        }
        $ramalan=$temp/$i;
        $defuzzifikasi[$n]=['relasi'=>$key['relasi'],'hasil'=>$key['hasil'],'ramalan'=>$ramalan];
        $n++;
      }
      return $defuzzifikasi;
    }


    public function relasi($flr)
    {
      $i=0;
      foreach ($flr as $key) {
        $temp="";
        if (!is_null($key['orde'])) {
          foreach ($key['orde'] as $key2) {
            $temp.=$key2.",";
          }
        }else {
          $temp=0;
        }


        $relasi[$i]=['period'=>$key['period'], 'month'=>$key['month'],'barang_name' => $key['barang_name'],'value'=>$key['value'], 'fuzzifikasi'=>$key['fuzzifikasi'],'orde'=>$key['orde'], 'relasi'=>$temp];
        $i++;
      }
      return $relasi;
    }


    public function relasi_defuzzifikasi($defuzzifikasi)
    {
      $i=0;
      foreach ($defuzzifikasi as $key) {
        $temp="";
        foreach ($key['relasi'] as $key2) {
          $temp.=$key2.",";
        }
        $relasi[$i]=['relasi'=>$temp, 'ramalan'=>$key['ramalan']];
        $i++;
      }
      return $relasi;
    }



    public function prediksi($flr,$defuzzifikasi)
    {
      $relasi=$this->relasi($flr);
      $relasi_deffuzifikasi=$this->relasi_defuzzifikasi($defuzzifikasi);
      $i=0;
      foreach ($relasi as $key) {
        if (!is_null($key['relasi'])) {
          foreach ($relasi_deffuzifikasi as $key2) {
            if ($key['relasi']==$key2['relasi']) {
              $prediksi[$i]=['period'=>$key['period'], 'month'=>$key['month'],'barang_name' => $key['barang_name'],'value'=>$key['value'], 'fuzzifikasi'=>$key['fuzzifikasi'],'orde'=>$key['orde'], 'relasi'=>$key['relasi'],'ramalan'=>$key2['ramalan']];
            }
          }
        }else {
          $prediksi[$i]=['period'=>$key['period'], 'month'=>$key['month'],'value'=>$key['value'], 'barang_name' => $key['barang_name'],'fuzzifikasi'=>$key['fuzzifikasi'],'orde'=>$key['orde'], 'relasi'=>$key['relasi'],'ramalan'=>null];
        }
        $i++;
      }
      return $prediksi;
    }

    public function kalkulasi_error($prediksi)
    {
      $i=0;
      foreach ($prediksi as $key) {
        if (!is_null($key['ramalan'])) {
          $absolute = abs($key['value']-$key['ramalan']);
          $afer = $absolute/$key['value'];
          $mse = pow($absolute,2);
        }else{
          $absolute = 0;
          $afer = 0;
          $mse =0;
        }
        $error[$i]=['absolute'=>$absolute,'afer'=>$afer,'mse'=>$mse,'period'=>$key['period'],'barang_name'=>$key['barang_name'], 'month'=>$key['month'], 'value'=>$key['value'], 'fuzzifikasi'=>$key['fuzzifikasi'],'orde'=>$key['orde'], 'relasi'=>$key['relasi'],'ramalan'=>$key['ramalan']];
        $i++;
      }
      return $error;
    }

    public function mean_error($error)
    {
      $afer=0;
      $mse=0;
      $i=0;
      foreach ($error as $key) {
        if (!is_null($key['absolute'])) {
          $afer += $key['afer'];
          $mse += $key['mse'];
          $i++;
        }
      }

      $mean_afer=$afer/$i;
      $mean_mse=$mse/$i;
      $rmse = sqrt($mean_mse);
      $error = ['afer'=>$mean_afer,'mse'=> $mean_mse,'rmse'=>$rmse];
      return $error;
    }

    public function hasil_terbaik($chen)
    {
        $model = "Chen";
        $akurasi = 100-(round($chen['afer']*100,2));
        $hasil =['model'=>$model,'akurasi'=>$akurasi];
      
      return $hasil;
    }

    public function hitung_get()
    {
      return redirect(route('administrator.dataset'));
    }


    public function getDataResult(Request $request)
    {
      $a=Dataset::orderBy('period', 'DESC')->first();

      $total_orde = (int)$a->period - (int)$request->period ;

      if($total_orde == 0){
        $total_orde = 1;
      }
      $orde = $total_orde;


      $jumlah_data= Dataset::where('barang_id', $request->barang_id)->count();
      $dataset=Dataset::where('barang_id', $request->barang_id)
      ->with(['barang'])
      ->get();
      if ($jumlah_data==0) {
        return redirect()->back()->with('error', 'Dataset belum ada!');
      }
      $data['dataset'] = $dataset;
      $data['user'] = User::find(Auth::user()->id);
      $k=(int)round(1+3.3*log10($jumlah_data));
      $data['k']=$k;
      $max=Dataset::max('value');
      $min=Dataset::min('value');
      $interval=round(($max-$min)/$k,2);
      $data['lebar']=$interval;
      $i=0;
      $temp=$min;
      for ($i=0;$i<$k;$i++) {
        $bawah=$temp;
        $temp+=$interval;
        $data['interval'][$i] = ['bawah'=>$bawah,'atas'=>$temp,'median'=>($bawah+$temp)/2];
      }

      $data['flr']=$this->fuzzifikasi($dataset,$data['interval'],$orde);

      $data['orde']=$orde;
      $data['flrg_chen']=$this->flrg_chen($data['flr']);
      $data['defuzzifikasi_chen']=$this->defuzzifikasi_chen($data['flrg_chen'],$data['interval']);
      $data['prediksi_chen']=$this->prediksi($data['flr'],$data['defuzzifikasi_chen']);
      $data['error_chen'] = $this->kalkulasi_error($data['prediksi_chen']);
      $data['mean_chen'] = $this->mean_error($data['error_chen']);
      $data['hasil'] = $this->hasil_terbaik($data['mean_chen']);
      $data['page_name'] = 'Hasil Perhitungan';
      $data['url_add'] = null;

      return view('admin.hasil.result',$data);
    }


}
