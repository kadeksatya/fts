<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Dataset;
use App\Models\Month;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DatasetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Dataset::select('id','period','month','barang_id','value')->with(['barang'])->get()->groupBy('period');
        $month = Dataset::select('month')->get()->groupBy('month');
        $query = Month::all();
        return view('admin.dataset.index',[
            'data' => $data,
            'query' => $query,
            'month' => $month,
            'page_name' => 'Datasets',
            'url_add' => '/admin/data/create'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = Barang::all();
        $year = range(2018, 2030);
        $month = Month::all();
        return view('admin.dataset.form', [
            'url_add' => null,
            'page_name' => 'Create Dataset',
            'data' => null,
            'year' => $year,
            'month' => $month,
            'barang' => $barang,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $request->all();


        Dataset::create($form);
        return redirect('/admin/data')->with('success','Data berhasil disimpan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $barang = Barang::all();

        $year = range(2018, 2030);
        $month = Month::all();
        $data = Dataset::whereId($id)->first();
        return view('admin.dataset.form', [
            'url_add' => null,
            'page_name' => 'Update Dataset',
            'data' => $data,
            'year' => $year,
            'barang' => $barang,
            'month' => $month
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $request->except('_method','_token');

        Dataset::whereId($id)->update($form);
        return redirect('/admin/data')->with('success','Data berhasil disimpan !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Dataset::whereId($id)->delete();

        return response()->json([
            'message' => 'Data Successfully Deleted !'
        ], 200);
    }
}
