<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Barang::orderBy('created_at', 'ASC')->get();

        return view('admin.bahan.index',[
            'data' => $data,
            'page_name' => 'Bahan',
            'url_add' => '/admin/bahan/create'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.bahan.form', [
            'url_add' => null,
            'page_name' => 'Create Bahan',
            'data' => null,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $request->all();

        Barang::create($form);
        return redirect('/admin/bahan')->with('success','Data berhasil disimpan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Barang::whereId($id)->first();
        return view('admin.bahan.form', [
            'url_add' => null,
            'page_name' => 'Update Bahan',
            'data' => $data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $request->except('_method','_token');

        Barang::whereId($id)->update($form);
        return redirect('/admin/bahan')->with('success','Data berhasil disimpan !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang::whereId($id)->delete();

        return response()->json([
            'message' => 'Data Successfully Deleted !'
        ], 200);
    }
}
