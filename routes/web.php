<?php

use App\Http\Controllers\BarangController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DatasetController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\ResultController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.auth.index', [
        'page_name' => 'Dashboard'
    ]);
});

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout.destroy');


Route::middleware('auth:web')->prefix('admin')->group(function(){
    Route::controller(DashboardController::class)->prefix('dashboard')->group(function(){
        Route::get('/', 'index')->name('dashboard.index');
    });

    Route::controller(DatasetController::class)->prefix('data')->group(function(){
        Route::get('/', 'index')->name('data.index');
        Route::get('/create', 'create')->name('data.create');
        Route::post('/store', 'store')->name('data.store');
        Route::get('/{id}/edit', 'edit')->name('data.edit');
        Route::put('/{id}/update', 'update')->name('data.update');
        Route::delete('/{id}/delete', 'destroy')->name('data.destroy');
    });

    Route::controller(BarangController::class)->prefix('bahan')->group(function(){
        Route::get('/', 'index')->name('bahan.index');
        Route::get('/create', 'create')->name('bahan.create');
        Route::post('/store', 'store')->name('bahan.store');
        Route::get('/{id}/edit', 'edit')->name('bahan.edit');
        Route::put('/{id}/update', 'update')->name('bahan.update');
        Route::delete('/{id}/delete', 'destroy')->name('bahan.destroy');
    });

    Route::controller(ResultController::class)->prefix('prediksi')->group(function(){
        Route::get('/', 'index')->name('prediksi.index');
        Route::get('/hasil', 'getDataResult');
    });
});