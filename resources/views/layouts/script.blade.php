<script src="{{asset('assets/lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/lib/feather-icons/feather.min.js')}}"></script>
<script src="{{asset('assets/lib/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('assets/lib/jquery.flot/jquery.flot.js')}}"></script>
<script src="{{asset('assets/lib/jquery.flot/jquery.flot.stack.js')}}"></script>
<script src="{{asset('assets/lib/jquery.flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('assets/lib/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('assets/lib/jqvmap/maps/jquery.vmap.usa.js')}}"></script>

<script src="{{asset('assets/js/dashforge.js')}}"></script>

<!-- append theme customizer -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="{{asset('assets/lib/chart.js/Chart.bundle.min.js')}}"></script>



<script>

    

    $(document).ready(function () {
        $('.error').show(function(){
            Swal.fire(
            'Opss',
            $(this).data('message'),
            'error'
            )
        })
        $('.success').show(function(){
            Swal.fire(
            'Horey',
            $(this).data('message'),
            'success'
            )
        })
    });
    $(document).on('click', '.delete-item', function () {
        var url = $(this).data('url');

        console.log(url);
        Swal.fire({
            title: 'Warning',
            text: "Are you sure for delete this ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Delete!'
        }).then((result) => {
            if (result.isConfirmed) {
                var id = $(this).data("id");
                var token = $("meta[name='csrf-token']").attr("content");
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data: {
                        "id": id,
                        "_token": token,
                    },
                    success: function (data) {
                        Swal.fire(
                            'Deleted!',
                            data.message,
                            'success'
                        )
                        window.location.reload().time(3)
                    }
                });
            }
        })
    })
</script>