<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{config('app.name')}} | {{$page_name}}</title>

    @include('layouts.style')

  </head>
  <body>
  
    @if (session('success'))
        <span class="success" data-message="{{session('success')}}"></span>
    @endif
    @if (session('error'))
      <span class="error" data-message="{{session('error')}}"></span>
    @endif

    @include('layouts.navbar')


    <div class="content ht-100v pd-0">
      <div class="content-header">
      </div><!-- content-header -->

      <div class="content-body">
        <div class="container pd-x-0">
          <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1 mg-b-10">
                  <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">{{$page_name}}</li>
                </ol>
              </nav>
              <h4 class="mg-b-0 tx-spacing--1">{{$page_name}}</h4>
            </div>
            <div class="d-none d-md-block">
              @if ($url_add != null)
              <a href="{{$url_add}}" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="file" class="wd-10 mg-r-5"></i> Tambah Dataset</a>
                  
              @endif
            </div>
          </div>

          @yield('content')
        </div><!-- container -->
      </div>
    </div>
    @include('layouts.script')

    @stack('script')

  </body>
</html>
