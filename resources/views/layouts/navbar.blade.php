<aside class="aside aside-fixed">
  <div class="aside-header">
    <a href="{{route('dashboard.index')}}" class="aside-logo">Resturant<span>Pop</span></a>
    <a href="" class="aside-menu-link">
      <i data-feather="menu"></i>
      <i data-feather="x"></i>
    </a>
  </div>
  <div class="aside-body">
    <div class="aside-loggedin">
      <div class="d-flex align-items-center justify-content-start">
        <a href="" class="avatar"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></a>
      </div>
      <div class="aside-loggedin-user">
        <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
          <h6 class="tx-semibold mg-b-0">{{Auth::user()->name}}</h6>
          <i data-feather="chevron-down"></i>
        </a>
        <p class="tx-color-03 tx-12 mg-b-0">{{Auth::user()->email}}</p>
      </div>
      <div class="collapse" id="loggedinMenu">
        <ul class="nav nav-aside mg-b-0">
          <li class="nav-item"><a href="{{route('logout.destroy')}}" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
        </ul>
      </div>
    </div><!-- aside-loggedin -->
    <ul class="nav nav-aside">
      <li class="nav-item"><a href="{{route('dashboard.index')}}" class="nav-link"><i data-feather="home"></i> <span>Dashboard</span></a></li>
      <li class="nav-item"><a href="{{route('data.index')}}" class="nav-link"><i data-feather="shopping-bag"></i> <span>Dataset</span></a></li>
      <li class="nav-item"><a href="{{route('prediksi.index')}}" class="nav-link"><i data-feather="calendar"></i> <span>Prediksi Barang</span></a></li>
      <li class="nav-label mg-t-25">Master Data</li>
      <li class="nav-item"><a href="{{route('bahan.index')}}" class="nav-link"><i data-feather="shopping-bag"></i> <span>Bahan</span></a></li>
    </ul>
  </div>
</aside>