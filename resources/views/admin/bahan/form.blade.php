@extends('layouts.app')
@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-6">
      <div class="card">

        <div class="card-body">
            <form action="{{$data == null ? '/admin/bahan/store':'/admin/bahan/'.$data->id.'/update'}}" method="post">
                @csrf
                @if ($data != null)
                @method('PUT')
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Nama Barang</label>
                            <input type="text" name="name" value="{{$data->name ?? ''}}" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Satuan Barang</label>
                            <input type="text" name="unit" value="{{$data->unit ?? ''}}" class="form-control" required>
                        </div>
                    </div>
                </div>
        </div>
        <div class="card-footer">
            @include('components.btn_action_form', [
                'url_back' => '/admin/bahan'
            ])
        </form>
        </div>

      </div>
    </div><!-- col -->
  </div><!-- row -->
@endsection