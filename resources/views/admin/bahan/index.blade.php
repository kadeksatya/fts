@extends('layouts.app')
@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-12">
      <div class="card">

        <div class="card-body">
            <table class="table table-bordered" style="width:100%;">
                <thead>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Satuan Barang</th>
                    <th width="20%" class="text-center">Action</th>
                </thead>
                <tbody>
                
                  @foreach ($data as $item)
                  <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->unit}}</td>
                    <td>
                      @include('components.btn_list',[
                        'url_edit' => route('bahan.edit', $item->id),
                        'url_delete' => route('bahan.destroy', $item->id),
                      ])
                    </td>
                  </tr>   
                  @endforeach
                  
                </tbody>
            </table>
        </div>

      </div>
    </div><!-- col -->
  </div><!-- row -->
@endsection