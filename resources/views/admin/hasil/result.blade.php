@extends('layouts.app')
@section('content')

<div class="col-xs-12">
    <div class="card">
        <div class="card-header">
            <p>Jumlah interval {{$k}} dan lebar Interval {{$lebar}}</p>
        </div>
        <div class="card-body">
            <table id="table1" class="table table-striped table-hover table-fw-widget">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Interval</th>
                        <th>Median</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach ($interval as $key)
                    <tr>
                        <td class="center">{{$i}}</td>
                        <td class="center">U{{$i}} = [{{$key['bawah']}} - {{$key['atas']}}]</td>
                        <td class="center">{{$key['median']}}</td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="col-xs-12 mt-3">

<div class="card">
    <div class="card-header">
        <p>Penggabaran Grafik</p>
    </div>
    <div class="card-body">
        <canvas id="chartLine1" height="300"></canvas>
    </div>
</div>

<div class="col-xs-12 mt-3">
    <div class="card">
        <div class="card-header">
            <p>Hasil Fuzzy Relationship Orde {{request()->get('period')}}</p>
        </div>
        <div class="card-body">
            <table id="table2" class="table table-striped table-hover table-fw-widget">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Jumlah</th>
                        <th>Fuzzifikasi</th>
                        <th>Relationship</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach ($flr as $key)
                    <tr>
                        <td class="center">{{$i}}</td>
                        <td class="center">{{$key['value']}}</td>
                        <td class="center">A{{$key['fuzzifikasi']}}</td>
                        @if (is_null($key['orde']))
                        <td class="center">-</td>
                        @else
                        <td class="center">
                            @foreach ($key['orde'] as $key2)
                            A{{$key2}},
                            @endforeach
                            ->A{{$key['fuzzifikasi']}}</td>
                        @endif
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

  
<div class="col-xs-12 mt-3">
    <div class="card">
        <div class="card-header">
            <p>FLRG Model Chen Berdasarkan Tahun {{request()->get('period')}}.</p>
        </div>
        <div class="card-body">
            <table id="table3" class="table table-striped table-hover table-fw-widget">
                <thead>
                    <tr>
                        <th>Grup</th>
                        <th>Relationship</th>
                        <th>FLRG</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach ($flrg_chen as $key)
                    <tr>
                        <td class="center">{{$i}}</td>
                        <td class="center">
                            @foreach ($key['relasi'] as $key2)
                            A{{$key2}},
                            @endforeach
                        </td>
                        <td class="center">
                            @foreach ($key['hasil'] as $key2)
                            A{{$key2}},
                            @endforeach
                        </td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="col-xs-12 mt-3">
    <div class="card">
        <div class="card-header">
            <p>Defuzzifikasi Model Chen Berdasarkan tahun {{request()->get('period')}}.</p>
        </div>
        <div class="card-body">
            <table id="table5" class="table table-striped table-hover table-fw-widget">
                <thead>
                    <tr>
                        <th>Grup</th>
                        <th>Relasi</th>
                        <th>FLRG</th>
                        <th>Hasil Ramalan</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach ($defuzzifikasi_chen as $key)
                    <tr>
                        <td class="center">{{$i}}</td>
                        <td class="center">
                            @foreach ($key['relasi'] as $key2)
                            A{{$key2}},
                            @endforeach
                        </td>
                        <td class="center">
                            @foreach ($key['hasil'] as $key2)
                            A{{$key2}},
                            @endforeach
                        </td>
                        <td class="center">{{$key['ramalan']}}</td>
                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="col-xs-12 mt-3">
    <div class="card">
        <div class="card-header">
            <p>Hasil Prediksi Model Chen Berdasarkan tahun {{request()->get('period')}}.</p>
        </div>
        <div class="card-body">
            <table id="table7" class="table table-striped table-hover table-fw-widget">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Month</th>
                        <th>Barang Name</th>
                        <th>Jumah</th>
                        <th>Fuzzifikasi</th>
                        <th>Relasi</th>
                        <th>Prediksi</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach ($prediksi_chen as $key)
                    <tr>
                        <td class="center">{{$i}}</td>
                        <td class="center">{{$key['month']}}</td>
                        <td class="center">{{$key['barang_name']}}</td>
                        <td class="center">{{$key['value']}}</td>
                        <td class="center">A{{$key['fuzzifikasi']}}</td>
                        <td class="center">
                            @if (!is_null($key['orde']))
                            @foreach ($key['orde'] as $key2)
                            A{{$key2}},
                            @endforeach
                            @else
                            -
                            @endif

                        </td>
                        @if (!is_null($key['orde']))
                        <td class="center">{{$key['ramalan']}}</td>
                        @else
                        <td class="center">-</td>
                        @endif

                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="col-xs-12 mt-3">
    <div class="card">
        <div class="card-header">
            <p>Kesalahan Prediksi Model Chen Berdasarkan tahun {{request()->get('period')}}.</p>
        </div>
        <div class="card-body">
            <table id="table10" class="table table-striped table-hover table-fw-widget">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Month</th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Hasil Ramalan</th>
                        <th>Nilai Absolute</th>
                        <th>AFER</th>
                        <th>MSE</th>

                    </tr>
                </thead>
                <tbody>
                    @php
                    $i=1;
                    @endphp
                    @foreach ($error_chen as $key)
                    <tr>
                        <td class="center">{{$i}}</td>
                        <td class="center">{{$key['month']}}</td>
                        <td class="center">{{$key['barang_name']}}</td>
                        <td class="center">{{$key['value']}}</td>
                        <td class="center">{{$key['ramalan']}}</td>

                        @if (!is_null($key['absolute']))
                        <td class="center">{{$key['absolute']}}</td>
                        <td class="center">{{$key['afer']}}</td>
                        <td class="center">{{$key['mse']}}</td>
                        @else
                        <td class="center">-</td>
                        <td class="center">-</td>
                        <td class="center">-</td>
                        @endif

                    </tr>
                    @php
                    $i++;
                    @endphp
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script>
        $(document).ready(function () {

  var ctxLabel =[ @foreach ($prediksi_chen as $item) "{{$item['month']}}",  @endforeach ] ;
  var ctxData1 = [@foreach ($prediksi_chen as $item) {{$item['value']}},  @endforeach];
  var ctxData2 = [@foreach ($prediksi_chen as $item) {{$item['ramalan']}},  @endforeach];
  var ctxColor1 = '#001737';
  var ctxColor2 = '#1ce1ac';
             // Line chart
  var ctx4 = document.getElementById('chartLine1');
  new Chart(ctx4, {
    type: 'line',
    data: {
      labels: ctxLabel,
      datasets: [{
        label:'Dataset',
        data: ctxData1,
        borderColor: ctxColor1,
        borderWidth: 1,
        fill: true
      },{
        label:'Ramalan',
        data: ctxData2,
        borderColor: ctxColor2,
        borderWidth: 1,
        fill: true
      }]
    },
    options: {
      maintainAspectRatio: false,
      legend: {
        display: true,
          labels: {
            display: true
          }
      },
      scales: {
        yAxes: [{
          gridLines: {
            color: '#e5e9f2'
          },
          ticks: {
            beginAtZero:true,
            fontSize: 10,
          }
        }],
        xAxes: [{
          gridLines: {
            display: true
          },
          ticks: {
            beginAtZero:true,
            fontSize: 11
          }
        }]
      }
    }
  });
        });
    </script>
@endpush