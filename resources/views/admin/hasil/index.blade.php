@extends('layouts.app')
@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-8">
      <div class="card">
        <div class="card-header">
          Prediksi Data
        </div>
        <div class="card-body">
          <form action="/admin/prediksi/hasil" method="GET">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="">Period</label>
                <select name="period" id="" class="form-control">
                @foreach ($year as $key => $value)
                  <option value="{{$value}}">{{$value}}</option>
                @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
              <label for="">Bahan</label>
              <select name="barang_id" id="" class="form-control">
                  <option value="" selected disabled></option>
                  @foreach ($bahan as $key => $value)
                  <option value="{{$value->id}}">{{$value->name}}</option>
                  @endforeach
              </select>
            </div>
            </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i> Cari Hasil</button>
        </div>
      </form>


      </div>
    </div><!-- col -->
  </div><!-- row -->
@endsection