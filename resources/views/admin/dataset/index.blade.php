@extends('layouts.app')
@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-12">
      <div class="card">

        <div class="card-body">
            <table class="table table-bordered" style="width:100%;">
                <thead>
                    <th width="10%">Periode</th>
                    <th width="15%">Bulan</th>
                    <th  width="30%">Nama Barang</th>
                    <th  width="10%">Satuan</th>
                    <th  width="10%">Jumlah</th>
                    <th width="15%">Action</th>
                </thead>
                <tbody>
                  
                  @foreach ($data as $key => $value)
                  <tr>
                    <td colspan="6">{{$key}}</td>
                  </tr>

                  @foreach ($month as $k => $v)
                  <tr>
                    <td></td>
                    <td colspan="5">{{$k}}</td>
                  </tr>
                  @foreach ($value as $item)
                  
                  @if ($item->month == $k && $item->period == $key)

                  <tr>
                    <td></td>

                    <td></td>
                    <td>{{$item->barang->name}}</td>
                    <td>{{$item->barang->unit}}</td>
                    <td>{{$item->value}}</td>
                    <td>
                      @include('components.btn_list',[
                        'url_edit' => "/admin/data/".$item->id."/edit",
                        'url_delete' => "/admin/data/".$item->id."/delete",
                      ])
                    </td>
                  </tr> 
                  @endif
                  @endforeach



                    
                  @endforeach
                  
                  @endforeach
                </tbody>
            </table>
        </div>

      </div>
    </div><!-- col -->
  </div><!-- row -->
@endsection