@extends('layouts.app')
@section('content')
<div class="row row-xs">
    <div class="col-sm-6 col-lg-6">
      <div class="card">

        <div class="card-body">
            <form action="{{$data == null ? '/admin/data/store':'/admin/data/'.$data->id.'/update'}}" method="post">
                @csrf
                @if ($data != null)
                @method('PUT')
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Nama Barang</label>
                            <select name="barang_id" id="" class="form-control">
                                <option value="" selected disabled></option>
                                @if ($data == null)
                                @foreach ($barang as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                                @else
                                @foreach ($barang as $item)
                                <option value="{{$item->id}}" {{$item->id == $data->barang_id ? 'selected':''}}>{{$item->name}}</option>
                                @endforeach
                                @endif
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="">Value</label>
                            <input type="number" step="any" name="value" value="{{$data->value ?? ''}}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Month</label>
                            <select name="month" id="" class="form-control">
                                <option value="" selected disabled></option>
                                @if ($data == null)
                                @foreach ($month as $item)
                                <option value="{{$item->name}}">{{$item->name}}</option>
                                @endforeach

                                @else
                                @foreach ($month as $item)
                                
                                <option value="{{$item->name}}" {{$item->name == $data->month ? 'selected':''}}>{{$item->name}}</option>
                                @endforeach

                                @endif
                            </select>

                        </div>
                        <div class="form-group">
                            <label for="">Period</label>
                            <select name="period" id="" class="form-control">
                                <option value="" selected disabled></option>
                                @if ($data == null)
                                @foreach ($year as $key => $value)
                                <option value="{{$value}}">{{$value}}</option>
                                @endforeach

                                @else
                                @foreach ($year as $key => $value)

                                <option value="{{$value}}" {{$value == $data->period ? 'selected':''}}>{{$value}}</option>
                                @endforeach

                                @endif
                            </select>

                        </div>
                    </div>
                </div>
        </div>
        <div class="card-footer">
            @include('components.btn_action_form', [
                'url_back' => '/admin/data'
            ])
        </form>
        </div>

      </div>
    </div><!-- col -->
  </div><!-- row -->
@endsection