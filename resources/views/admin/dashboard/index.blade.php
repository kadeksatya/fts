@extends('layouts.app')
@section('content')

    <div class="row row-xs">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex">
                        <div class="flex-1 overflow-hidden">
                            <p class="text-truncate font-size-14 mb-2">Jumlah Barang</p>
                            <h4 class="mb-0">{{$total_barang}} Barang</h4>
                        </div>
                        <div class="text-primary ms-auto">
                            <i class="ri-stack-line font-size-24"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-3">
          <div class="card">
              <div class="card-body">
                <canvas id="chartBar1" height="300"></canvas>
              </div>
          </div>
      </div>
    </div><!-- row -->
@endsection

@push('script')
    <script>
        $(document).ready(function () {

  var ctxLabel =[ @foreach ($data_barang as $key => $value) "{{$key}}",  @endforeach ] ;
  var ctxData1 = [@foreach ($data_barang as $key => $value) {{$value}},  @endforeach];
  var ctxColor1 = '#001737';
  var ctxColor2 = '#1ce1ac';
  
    // Bar chart
    var ctx1 = document.getElementById('chartBar1').getContext('2d');
    new Chart(ctx1, {
    type: 'bar',
    data: {
      labels: ctxLabel,
      datasets: [
      {
        data: ctxData1,
        backgroundColor: ctxColor1
      }
    ]
    },
    options: {
      maintainAspectRatio: false,
      responsive: true,
      legend: {
        display: false,
        labels: {
          display: false
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            color: '#e5e9f2'
          },
          ticks: {
            beginAtZero:true,
            fontSize: 10,
            fontColor: '#182b49',
          }
        }],
        xAxes: [{
          gridLines: {
            display: false
          },
          barPercentage: 0.6,
          ticks: {
            beginAtZero:true,
            fontSize: 11,
            fontColor: '#182b49'
          }
        }]
      }
    }
  });
        });
    </script>
@endpush
