@if ($url_edit != null)
    <a href="{{$url_edit}}" class="btn btn-primary">Edit</a>
@endif
@if ($url_delete != null)
    <button type="button" class="btn btn-danger delete-item" data-url="{{$url_delete}}">Delete</button> 
@endif