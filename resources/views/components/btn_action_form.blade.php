<div class="float-right">
    <a href="{{$url_back}}" class="btn btn-dark"><i data-feather="skip-back"></i> Back</a>
    <button class="btn btn-primary"><i data-feather="save"></i> Submit</button>
</div>